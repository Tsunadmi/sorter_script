# Sorter_script

Sorter script for sort file of Download folder

# Installation

You need install python 3.9:

```
pip install python

```

# Import important libraries

```
import os.path
```

# Getting started

Start of script:

```
python script.py
```

# Technologies used

- Python
- JetBrains
- Os

## Description

This script helps to sort data from Download folder to different folders(for example: pdf, jpg, docx etc).

# Author

This script was done by Dmitry Tsunaev.

- [LinkedIn](https://www.linkedin.com/in/dmitry-tsunaev-530006aa/)
