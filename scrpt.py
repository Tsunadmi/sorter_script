import os
import os.path

if not os.path.exists("C:/Sort_folder/doc_folder"):
    os.makedirs("C:/Sort_folder/doc_folder")
if not os.path.exists("C:/Sort_folder/pic_folder"):
    os.makedirs("C:/Sort_folder/pic_folder")
if not os.path.exists("C:/Sort_folder/music_folder"):
    os.makedirs("C:/Sort_folder/music_folder")
if not os.path.exists("C:/Sort_folder/video_folder"):
    os.makedirs("C:/Sort_folder/video_folder")
if not os.path.exists("C:/Sort_folder/other_folder"):
    os.makedirs("C:/Sort_folder/other_folder")
if not os.path.exists("C:/Sort_folder/work_tools_folder"):
    os.makedirs("C:/Sort_folder/work_tools_folder")

downloads_folder = "C:/Users/tsunaev/Downloads"
other_folder = "C:/Sort_folder/other_folder"
video_folder = "C:/Sort_folder/video_folder"
doc_folder = "C:/Sort_folder/doc_folder"
pic_folder = "C:/Sort_folder/pic_folder"
work_tools_folder = "C:/Sort_folder/work_tools_folder"


for filename in os.listdir(downloads_folder):
    exist = filename.split('.')
    if len(exist)==2 and exist[1].lower() == 'pdf' or len(exist)==2 and exist[1].lower() == 'doc' or len(exist)==2 and\
            exist[1].lower() == 'xlsx' or len(exist)==2 and exist[1].lower() == 'xlsm' or len(exist)==2 and exist[1].lower() == 'docx':
        file = downloads_folder +'/'+ filename
        new_folder = doc_folder +'/'+ filename
        os.rename(file, new_folder)
    elif len(exist)==2 and exist[1].lower() == 'jpeg' or len(exist)==2 and exist[1].lower() == 'jpg':
        file = downloads_folder +'/'+ filename
        new_folder = pic_folder +'/'+ filename
        os.rename(file, new_folder)
    elif len(exist)==2 and exist[1].lower() == 'mp4':
        file = downloads_folder +'/'+ filename
        new_folder = video_folder +'/'+ filename
        os.rename(file, new_folder)
    elif len(exist)==2 and exist[1].lower() == 'step':
        file = downloads_folder +'/'+ filename
        new_folder = work_tools_folder +'/'+ filename
        os.rename(file, new_folder)
    else:
        file = downloads_folder +'/'+ filename
        new_folder = other_folder +'/'+ filename
        os.rename(file, new_folder)